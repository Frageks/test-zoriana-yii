<?php

namespace app\controllers;

use app\models\Translations;
use Yii;
use app\models\Categories;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * CategoriesController implements the CRUD actions for Categories model.
 */
class CategoriesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors ()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categories models.
     * @return mixed
     */
    public function actionIndex ()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Categories::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Categories model.
     * @param integer $id
     * @return mixed
     */
    public function actionView ($id)
    {
        $model = $this->findCategoryModel($id);
        return $this->render('view', [
            'model'            => $model,
            'parent_title'     => Categories::getParentTitle($model),
            'translation_data' => Translations::getTranslationsData($model),
        ]);
    }

    /**
     * Creates a new Categories model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate ()
    {
        $model = new Categories();
        $translate_model = new Translations();
        if (!empty(Yii::$app->request->post())) {
            $data = Yii::$app->request->post();
            $model->parent_id = $data[$model->formName()]['parent_id'];
            unset($data[$model->formName()]['parent_id']);
            if ($model->save()) {
                foreach ($data[$translate_model->formName()] as $key => $datum) {
                    $translates_model = new Translations();
                    $translates_model->language = $key;
                    $translates_model->category_id = $model->id;
                    $translates_model->title = $datum['title'];
                    $translates_model->description = $datum['description'];
                    $translates_model->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model'            => $model,
            'translates_model' => $translate_model,
        ]);
    }

    /**
     * Updates an existing Categories model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate ($id)
    {
        $model = $this->findCategoryModel($id);
        $translate_models = Translations::findModel(['category_id' => $id]);
        $data = Yii::$app->request->post();
        if (!empty($data)) {
            $model->parent_id = $data[$model->formName()]['parent_id'];
            if ($model->save()) {
                foreach ($data[$translate_models[0]->formName()] as $key => $datum) {
                    $translates_model = Translations::findModel(['language' => $key, 'category_id' => $model->id]);
                    $translates_model->title = $datum['title'];
                    $translates_model->description = $datum['description'];
                    $translates_model->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Categories model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete ($id)
    {
        $this->findCategoryModel($id)->delete();
        $translations = Translations::findModel(['category_id' => $id]);
        foreach ((array)$translations as $translation) {
            $translation->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $id
     * @return Categories|\yii\db\ActiveRecord
     */
    private function findCategoryModel ($id)
    {
        return Categories::findModel($id);
    }
}
