<?php

use yii\db\Migration;

/**
 * Handles the creation of table `translations`.
 */
class m170510_201555_create_translations_table extends Migration
{
    private $_table_name = 'translations';

    /**
     * @inheritdoc
     */
    public function up ()
    {
        $this->createTable($this->_table_name, [
            'id'          => $this->primaryKey(),
            'language'    => $this->string()->defaultValue('en-GB')->notNull(),
            'category_id' => $this->integer()->notNull(),
            'title'       => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
        ]);
        $this->addForeignKey('fk-translations-category_id-categories-id', $this->_table_name, 'category_id',
            'categories', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down ()
    {
        $this->dropTable('translations');
        $this->dropForeignKey('fk-translations-category_id-categories-id', $this->_table_name);
    }
}
