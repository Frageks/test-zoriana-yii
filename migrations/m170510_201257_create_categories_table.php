<?php

use yii\db\Migration;

/**
 * Handles the creation of table `categories`.
 */
class m170510_201257_create_categories_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up ()
    {
        $this->createTable('categories', [
            'id'        => $this->primaryKey(),
            'parent_id' => $this->integer()->defaultValue(0)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down ()
    {
        $this->dropTable('categories');
    }
}
