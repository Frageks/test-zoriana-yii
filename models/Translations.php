<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%translations}}".
 *
 * @property integer $id
 * @property string $language
 * @property integer $category_id
 * @property string $title
 * @property string $description
 *
 * @property Categories $category
 */
class Translations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%translations}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'category_id', 'title', 'description'], 'required'],
            [['category_id'], 'integer'],
            [['description'], 'string'],
            [['language', 'title'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language' => Yii::t('app', 'Language'),
            'category_id' => Yii::t('app', 'Category ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    /**
     * @param array $condition
     *
     * @return null|static|static[]
     */
    static public function findModel (Array $condition)
    {
        if (key_exists('category_id', $condition) && key_exists('language', $condition)) {
            return self::findOne($condition);
        } elseif (key_exists('category_id', $condition) && !key_exists('language', $condition)) {
            return self::findAll($condition);
        }
        return null;
    }

    /**
     * Returns translations data.
     *
     * @param Categories $model
     * @return array|null
     */
    static public function getTranslationsData (Categories $model)
    {
        if (!is_null($model->translations)) {
            foreach ($model->translations as $key => $translation) {
                if ($translation['language'] === Yii::$app->language) {
                    return [
                        'title'       => $translation['title'],
                        'description' => $translation['description'],
                    ];
                }
            }
        }
        return null;
    }

    static public function getAllLanguages ()
    {
        return [
            'en-GB' => 'English',
            'uk-UA' => 'Українська',
        ];
    }
}
