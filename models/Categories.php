<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $id
 * @property integer $parent_id
 *
 * @property Translations[] $translations
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%categories}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['parent_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translations::className(), ['category_id' => 'id']);
    }

    /**
     * Static function for fetching all categories excepts current
     *
     * @param int|null $except_id
     * @return array|ActiveRecord[]
     */
    static public function getAllCategories (int $except_id = null)
    {
        $result = [];
        $rows = is_null($except_id) ?
            self::find()->with('translations')->all() :
            self::find()->with('translations')->where(['<>', 'id', $except_id])->all();

        foreach ((array)$rows as $row) {
            foreach ((array)$row->translations as $translation) {
                if ($translation->language === Yii::$app->language) {
                    $result[$row->id] = $translation->title;
                }
            }
        }
        return $result;
    }

    /**
     * Finds the Categories model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $id
     * @return Categories|ActiveRecord
     * @throws NotFoundHttpException
     */
    static public function findModel ($id)
    {
        $model = self::find()->where(['id' => $id])->with('translations')->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds title of parent category in current language.
     *
     * @param Categories $model
     * @return null
     */
    static public function getParentTitle (Categories $model)
    {
        $translations = self::findOne($model->parent_id);
        $parent_title = null;
        if (!is_null($translations)) {
            foreach ((array)$translations->translations as $key => $translation) {
                if ($translation['language'] === Yii::$app->language) {
                    return $translation['title'];
                    break;
                }
            }
        }
        return null;
    }
}
