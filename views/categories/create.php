<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $translates_model \app\models\Translations */

$this->title = Yii::t('app', 'Create Categories');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model'           => $model,
        'model_translate' => $translates_model,
    ]) ?>

</div>
