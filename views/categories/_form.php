<?php

use app\helpers\FormTranslate;
use app\models\Translations;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */
/* @var $model_translate Translations */
$languages = Translations::getAllLanguages();

?>

<div class="categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'parent_id')->dropDownList(['0' => 'No Parent'] +
            Categories::getAllCategories($model->id)) ?>

    <ul class="nav nav-pills">
        <?php foreach ((array)$languages as $k => $lang) { ?>
            <li class=" <?= ($k == Yii::$app->language) ? "active" : '' ?>">
                <a data-toggle="pill" href="#translate<?= $k ?>">
                    <?= Yii::t( 'app', 'Translation ({lang})', ['lang' => $lang] ); ?>
                </a>
            </li>
        <?php } ?>
    </ul>

    <div class="tab-content">
        <?php foreach ((array)$languages as $k => $lang) {
            if (!$model->isNewRecord) {
                $model_translate = FormTranslate::getTranslateModel(Translations::className(), $k,
                    ['category_id' => $model->id]);
            } ?>
            <div id="translate<?= $k ?>" class="tab-pane fade <?= ($k == Yii::$app->language) ? "in active" : '' ?>">
                <?= $form->field($model_translate, "[{$k}]title")->textInput(['maxlength' => true]) ?>
                <?= $form->field($model_translate, "[{$k}]description")->textarea(['rows' => 4]) ?>
            </div>
        <?php } ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
