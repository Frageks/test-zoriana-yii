<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $model_translate \app\models\Translations */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Categories',
    ]) . \app\models\Translations::getTranslationsData($model)['title'];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="categories-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
