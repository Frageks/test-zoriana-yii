<?php

use app\models\Categories;
use app\models\Translations;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Categories'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'title',
                'value'     => function ($model) {
                    $data = Translations::getTranslationsData($model);
                    return is_null($data) ? null : $data['title'];
                },
            ],
            [
                'attribute' => 'description',
                'value'     => function ($model) {
                    $data = Translations::getTranslationsData($model);
                    return is_null($data) ? null : $data['description'];
                },
            ],
            [
                'attribute' => 'parent_id',
                'value'     => function ($model) {
                    return Categories::getParentTitle($model);
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
