<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $parent_title string */
/** @var $translation_data array */

$this->title = Yii::t('app', 'View {modelClass}: ', [
        'modelClass' => 'Categories',
    ]) . \app\models\Translations::getTranslationsData($model)['title'];
$this->params['breadcrumbs'][] = Yii::t('app', 'View');

?>
<div class="categories-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data'  => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method'  => 'post',
            ],
        ]) ?>
    </p>
    <?= DetailView::widget([
        'model'      => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'title',
                'value'     => key_exists('title', (array)$translation_data) ? $translation_data['title'] : null,
            ],
            [
                'attribute' => 'description',
                'value'     => key_exists('description', (array)$translation_data) ? $translation_data['description'] : null,
            ],
            [
                'attribute' => 'parent_id',
                'value'     => $parent_title,
            ],
        ],
    ]) ?>

</div>
