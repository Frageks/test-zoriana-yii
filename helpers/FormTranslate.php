<?php

namespace app\helpers;

use yii\base\Object;
use yii\db\ActiveRecord;

/**
 * Class FormTranslate
 *
 * @package common\helpers
 */
class FormTranslate extends Object
{
    /**
     * @param string $class
     * @param string $id_language
     * @param array $options
     *
     * @return mixed
     */
    public static function getTranslateModel($class, $id_language, $options = [])
    {
        /** @var ActiveRecord $class */
        $model = $class::find()->where(['language' => $id_language])->andWhere($options)->one();
        return $model ? $model : new $class;
    }
}
