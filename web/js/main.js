/**
 * Created by frageks on 09.05.17.
 */

var changeLanguage = function () {
    "use strict";
    var $el = document.getElementById('languages');
    var val = $el.options[$el.selectedIndex].value;
    var oldUrlSegments = document.location.href.split('/');
    oldUrlSegments.splice(3, 1, val);
    document.location.href = oldUrlSegments.join('/');
};